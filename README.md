# bash-blaster
Un script de Bash que proporciona una GUI básica para blast y permite filtrar el output por criterios de coverage e identity

A bash script that provides a basic GUI for blast and helps filter the output by coverage and identity values

## Descarga
Para usar este método, es necesario tener instalado git en el sistema (`sudo apt install git-all`)
1. Clona este repositorio: `git clone https://codeberg.org/FlyingFlamingo/bash-blaster.git`
2. Entra en el directorio: `cd bash-blaster`
3. Ejecútalo

## Opciones
Al llamar al script principal, tenemos las siguientes opciones:

| Opción | Descripción |
| -------- | -------- |
| ``--help | -h`` | Muestra la ayuda |
| ``--options`` | Muestra las opciones del script |
| ``--license`` | Muestra la licencia |
| ``--version`` | Muestra la versión |