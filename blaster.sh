#!/usr/bin/env bash
#PSA: Si buscas ayuda, no busques más: usa la opción -h!

#Purgamos logs antiguos
if test -f logs.txt; then
	rm logs.txt
fi

echo "El script comenzó a ejecutarse el" $(date '+%Y-%m-%d') "a las" $(date +"%H:%M:%S") >> logs.txt

#El profe nos ha dicho que definamos las opiones del case como funciones, así que allá voy
ayudame() {
    echo "Recuerda: para llamarme, debes usar ./blaster.sh, acompañado de los siguientes 4 argumentos"
    echo -e "\t1. input.fasta:\t\t el archivo .fasta query"
    echo -e "\t2. input.multifasta:\t el archivo .fasta subject"
    echo -e "\t3. coverage cut off:\t expresado como %, de forma NN.NN, valor no incluido"
    echo -e "\t4. identity cut off:\t expresado como %, de forma NN.NN, valor no incluido"
    echo "Recuerda: Los argumentos deben proporcionarse en el orden indicado, y los números, con \".\" en vez de \",\"; I dont speak numeracion españolo"
    echo "Si ejecutas este programa con -h obtendrás ayuda; con --options las opciones, y, con --license, la licencia"
    echo "Si deseas consultar el archivo de logs, usa nano logs.txt"
}

licencia() {
	echo "This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version."
	echo
	echo "This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
	echo
	echo "You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>."
}

version() {
	echo "Version: 1.0 - Copyright 2020 Pablo Ignacio Marcos"
        echo "Programado usando GNU Nano"
}

: ' Ahora, interpretemos las opciones. He puesto los exits aquí en vez de en la función por si
quisiera reutilizar la función sin terminar el proceso en sí. Por cierto, he aprendido a hacer 
multiline comments!'

case $1 in
	-h | --help)
	ayudame
	echo "El script terminó adecuadamente tras mostrar la ayuda" >> logs.txt
	exit 0
	;;
	--license)
	licencia
	echo "El script terminó adecuadamente tras mostrar la licencia" >> logs.txt
	exit 0
	;;
	--version)
	version
	echo "El script terminó adecuadamente tras mostrar la versión" >> logs.txt
	exit 0
	;;
	--options)
	echo -e "-h | --help: \tMuestra una ayuda completa"
	echo -e "--license: \tMuestra la licencia, pues mi creador <3 el free software"
	echo -e "--version: \tMuestra la versión"
	echo -e "--options: \tMuestra las opciones"
	echo -e "El script terminó adecuadamente tras mostrar las opciones" >> logs.txt
	exit 0
	;;
esac

#Si no hemos solicitado ninguna opción especial, comprobenos que los parámetros son OK
if [ $# -ne 4 ] ; then
	echo "Error: Debes usar 4 parámetros"
	echo "El script terminó inesperadamente: número inadecuado de parámetros" >> logs.txt
	exit 1
elif [ ! -f $1 ] || [ ! -f $2 ]; then
	echo "Error: El archivo no existe"
	echo "El script terminó inesperadamente: deben introducirse archivos válidos" >> logs.txt
	exit 1
elif [[ ! $3 =~ [0-9]+(\.[0-9][0-9]?)? ]] || [[ ! $4 =~ [0-9]+(\.[0-9][0-9]?)? ]]; then
        echo "Error: Debes proporcionar parámetros numéricos para identidad y coverage"
        echo "El script terminó inesperadamente: identidad y coverage deben ser números" >> logs.txt
        exit 1
else 
	echo Hola! Soy Trinidad, tu asistente offline para queries relacionadas con blast
fi

echo
echo Pero claro, para que todo funcione, ¡me debes proporcionar los archivos en formato fasta!
echo Deja que mis súper algoritmos comprueben que todo marcha bien
sleep 1
echo ...
sleep 1

fasta1=$(cat $1 | head -n 1)
fasta2=$(cat $2 | head -n 1)
if [[ $fasta1 = '>'* && $fasta2 = '>'* ]] #Comprobamos que sean fastas
then
	echo "Bueno, no te voy a engañar, mis algoritmos no son tan complejos. Simplemente he comprobado que tuviese la cabecera fasta (>). Y,¡enhorabuena, sí que la tiene! Así, sí."
	echo "Se proporcionaron archivos en el formato adecuado" >> logs.txt
else
	echo Pero bueno! Si esto no es un archivo fasta! Yo en estas condiciones, me niego a trabajar
	echo "El script terminó inesperadamente, ofendido por un fallo en el input" >> logs.txt
	exit 1
fi

echo
echo Ahora necesito saber cómo quieres llamar al directorio del proyecto. Recuerda no usar un nombre que ya exista!
read ID

if [ -d "$ID" ]; 
then
	echo "Este directorio ya existe. Por favor, escoge otro. Puedes ver una lista de los directorios existentes:"
	ls -d */
	echo "ERROR: El directorio seleccionado por el usuario ya existe" >> logs.txt
	exit 1
else	
	mkdir $ID $ID/data $ID/results
	cp $1 $2 ./$ID/data/  #El profe ha dicho que mejor cp que mv
	#Un breve comentario: OJO con el ./ para especificar la ruta relativa, me ha costado darme cuenta
	echo "El usuario ha especificado $ID como directorio del proyecto" >> logs.txt
fi

#Podría haberlo colocado en el awk más abajo, pero así queda más legible y coherente temporalmente
echo El usuario ha establecido los siguientes parámetros:
echo "*******************  Parámetros  *******************"
echo -e "Criterio de Identidad:\t $4" # -e permite interpretar los backslash escapes
echo -e "Criterio de Coverage:\t $3"
echo -e "Archivo Query:\t\t $1"
echo -e "Archivo Subject:\t $2"
echo "****************************************************"
echo
echo Procesando...

blastp -query $1 -subject $2 -evalue "0.000001" -outfmt "6 qseqid qcovs pident evalue sseqid sseq" > out_blast.tsv
echo "blastp se ejecutó con éxito" >>  logs.txt

#Vamos a preparar el formato del archivo out
quitaextensiones=$(echo "$1" | cut -f 1 -d '.')

#Heureusement, el blast ya me da los resultados como tsv, así que no hay que cambiarlo
awk -F"\t" -v cover=$3 -v ident=$4 '
{
if ($2>cover && $3>ident) {
	#Para ir generando el archivo de salida
	print $0 >> "out.tsv"
	#Y para ir calculando las variables de output
	mincov =100; minid=100; hits += 1; if (maxid < $3){ maxid = $3 }; if(minid > $3){ minid = $3 };
	if (maxcov < $2){ maxcov = $2 }; if(mincov > $2){ mincov = $2 }
	}
}
#Ahora printeo el stdoutput
END{
	print ""
	print "Los resultados obtenidos han sido:"
	print "**************  Resultados Obtenidos  **************"
	print "Número de Hits Obtenidos:\t" hits
	print "Rango de Identidad:\t\t" minid "-" maxid
	print "Rango de Coverage:\t\t" mincov "-" maxcov
	print "****************************************************"
}' out_blast.tsv
rm out_blast.tsv #keep it clean (podría haber hecho un pipe, pero no quería la cosa más larga del mundo

#Ahora que tengo el arhivo result, voy a procesarlo 

echo
mv out.tsv ./$ID/results/${quitaextensiones}_result.tsv
echo "El output file se ha almacenado en el directorio ./$ID/results/, bajo el nombre ${quitaextensiones}_result.tsv"
echo "El script terminó adecuadamente el" $(date '+%Y-%m-%d') "a las" $(date +"%H:%M:%S") >> logs.txt
exit 0
